import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Data} from './models/data';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataCollection : AngularFirestoreCollection<Data>;
  private data : Observable<Data[]>;

  constructor(db: AngularFirestore) {
    this.dataCollection = db.collection<Data>('data');
    this.data = this.dataCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
  }
  getAllData() {
    return this.data;
  }
  getData(id:string) {
    return this.dataCollection.doc<Data>(id).valueChanges();
  }  
}

