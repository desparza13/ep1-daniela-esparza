import { Component, OnInit } from '@angular/core';
import { Data } from '../models/data';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  data: Data[];
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.dataService.getAllData().subscribe(res => {
      console.log('Data', res);
      this.data = res;
    });
  }

  


}
